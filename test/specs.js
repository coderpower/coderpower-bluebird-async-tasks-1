var chai = require('chai');
var expect = chai.expect;
var promiseProps = require('../sources/promiseProps');
var promiseSome = require('../sources/promiseSome');

describe('promiseProps', function() {

    it('Must return a Promise', function(cb) {
        var result = promiseProps().then(function(){
            cb();
        })
    });

});

describe('promiseSome', function() {

    it('Must return a Promise', function(cb) {
        var result = promiseSome().then(function(){
            cb();
        })
    });

});
