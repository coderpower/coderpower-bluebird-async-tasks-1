### Async Tasks 1

Welcome to this first part of async tasks on collections.
We’ll begin using the props method of Bluebird.
Given a list of key/Promise pairs, it will return a promise, resolved when all Promise provided as key’s value are resolved. Props will eventually be fulfilled with an object with results of keys as value.
We’ll begin writing a function returning a promise which will resolved later with its resolved delay

```javascript
    function promiseResolvingAfter(delay) {

        return new Promise(function(resolve) {
            setTimeout(function() {
                resolve(delay)
            }, delay)
        })

    }
```

Then, we will use Promise.props with an object which has different Promises values for each key:

```javascript
    return Promise.props({
        hundredMilliseconds: promiseResolvingAfter(100),
        fiveHundredMilliseconds: promiseResolvingAfter(500),
        thousandMilliseconds: promiseResolvingAfter(1000)
    }).then(function(returningTimes) {
        console.log('hundredMilliseconds : ' + returningTimes.hundredMilliseconds);
        console.log('fiveHundredMilliseconds : ' + returningTimes.fiveHundredMilliseconds);
        console.log('thousandMilliseconds : ' + returningTimes.thousandMilliseconds);
    });
```

and add a “then” clause which will log promises resolving times.
Here we go, :

* the key “ hundred milliseconds” has a resolving time of 100.
* the key “ five hundred milliseconds” has a resolving time of 500
* and the key “ thousand milliseconds” has a resolving time of 1000.

Now, let’s say we want to retrieve only the first resolving promise. We’ll use Promise.some(), providing as first argument an array of promises, and as the second one the n fastest to be resolved:

```javascript
    return Promise.some([
        promiseResolvingAfter(100),
        promiseResolvingAfter(500),
        promiseResolvingAfter(40),
        promiseResolvingAfter(200)
    ], 1).spread(function(lowestTime) {
        console.log('The fastest resolving time for those promises is ' + lowestTime)
    });
```

Which will output : The fastest resolving time for those promises is 40.
Note that Promise.spread is used : it flattened an array to inline parameters to a function.
If you had 3 as a second parameter of some, spread would make inner function to be taken 3

arguments:

The fastest to be resolved, the second one, and the third one


For more information, please refer to the documentation: http://bluebirdjs.com/docs/api-reference.html
