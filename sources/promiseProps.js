var Promise = require("bluebird");

module.exports = function doSomething() {

    function promiseResolvingAfter(delay) {

        return new Promise(function(resolve) {
            setTimeout(function() {
                resolve(delay)
            }, delay)
        })

    }

    return Promise.props({
        hundredMilliseconds: promiseResolvingAfter(100),
        fiveHundredMilliseconds: promiseResolvingAfter(500),
        thousandMilliseconds: promiseResolvingAfter(1000)
    }).then(function(returningTimes) {
        console.log('hundredMilliseconds : ' + returningTimes.hundredMilliseconds);
        console.log('fiveHundredMilliseconds : ' + returningTimes.fiveHundredMilliseconds);
        console.log('thousandMilliseconds : ' + returningTimes.thousandMilliseconds);
    });

};