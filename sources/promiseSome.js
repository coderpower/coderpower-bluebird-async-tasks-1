var Promise = require("bluebird");

module.exports = function doSomething() {

    function promiseResolvingAfter(delay) {

        return new Promise(function(resolve) {
            setTimeout(function() {
                resolve(delay)
            }, delay)
        })

    }

    return Promise.some([
        promiseResolvingAfter(100),
        promiseResolvingAfter(500),
        promiseResolvingAfter(40),
        promiseResolvingAfter(200)
    ], 1).spread(function(lowestTime) {
        console.log('The fastest resolving time for those promises is ' + lowestTime)
    });

};